# Overview:

Perl Compatible Regular Expressions (PCRE) is a regular expression C library inspired by the regular expression capabilities in the Perl programming language. Philip Hazel started writing PCRE in summer 1997.[1] PCRE's syntax is much more powerful and flexible than either of the POSIX regular expression flavors and than that of many other regular-expression libraries.

## Homepage

[pcre project](http://www.pcre.org)

## Notifications (updates):

## Changes done:

## TODO:

